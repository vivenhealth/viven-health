import { VivenPage } from './app.po';

describe('viven App', () => {
  let page: VivenPage;

  beforeEach(() => {
    page = new VivenPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

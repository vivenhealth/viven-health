const functions = require( 'firebase-functions' );
const admin = require( 'firebase-admin' );
admin.initializeApp( functions.config().firebase );

// onCreateTrigger
exports.updateUserData = functions.auth.user().onCreate( event => {
	const database = admin.database();
	const userRef = this.database.ref( '/users' ).child( event.data.uid );
	const user = event.data;
	if ( user && user.id ) {
		console.log( 'Firebase Function updateUserData Activated', user );
		return userRef.update( user );
	}
} );

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
	production: false,
	firebaseConfig: {
		apiKey: 'AIzaSyAXIF070u2AKEOgvtBwgm9CO8JLUZD6i88',
		authDomain: 'viven-health-master.firebaseapp.com',
		databaseURL: 'https://viven-health-master.firebaseio.com',
		projectId: 'viven-health-master',
		storageBucket: 'viven-health-master.appspot.com',
		messagingSenderId: '431801304402'
	}
};

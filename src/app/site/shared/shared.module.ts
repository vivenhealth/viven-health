import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
	MdAutocompleteModule,
	MdButtonModule,
	MdButtonToggleModule,
	MdCardModule,
	MdCheckboxModule,
	MdChipsModule,
	MdDatepickerModule,
	MdDialogModule,
	MdGridListModule,
	MdIconModule,
	MdInputModule,
	MdListModule,
	MdMenuModule,
	MdProgressBarModule,
	MdProgressSpinnerModule,
	MdRadioModule,
	MdSelectModule,
	MdSlideToggleModule,
	MdSliderModule,
	MdSnackBarModule,
	MdTabsModule,
	MdToolbarModule,
	MdTooltipModule
} from '@angular/material';

/**
 * NgModule that includes all Material modules that are required for the app (Needs pruning)
 */
@NgModule( {
	imports: [
		MdAutocompleteModule,
		MdButtonModule,
		MdButtonToggleModule,
		MdCardModule,
		MdCheckboxModule,
		MdChipsModule,
		MdDatepickerModule,
		MdDialogModule,
		MdGridListModule,
		MdIconModule,
		MdInputModule,
		MdListModule,
		MdMenuModule,
		MdProgressBarModule,
		MdProgressSpinnerModule,
		MdRadioModule,
		MdSelectModule,
		MdSlideToggleModule,
		MdSliderModule,
		MdSnackBarModule,
		MdTabsModule,
		MdToolbarModule,
		MdTooltipModule
	],
	exports: [
		MdAutocompleteModule,
		MdButtonModule,
		MdButtonToggleModule,
		MdCardModule,
		MdCheckboxModule,
		MdChipsModule,
		MdDatepickerModule,
		MdDialogModule,
		MdGridListModule,
		MdIconModule,
		MdInputModule,
		MdListModule,
		MdMenuModule,
		MdProgressBarModule,
		MdProgressSpinnerModule,
		MdRadioModule,
		MdSelectModule,
		MdSlideToggleModule,
		MdSliderModule,
		MdSnackBarModule,
		MdTabsModule,
		MdToolbarModule,
		MdTooltipModule
	]
} )
export class MaterialModule { }

import { AwesomePipe } from './awesome.pipe';
import { HighlightDirective } from './highlight.directive';

@NgModule( {
	imports: [
		CommonModule,
		MaterialModule
	],
	declarations: [
		AwesomePipe,
		HighlightDirective
	],
	exports: [
		AwesomePipe,
		HighlightDirective,
		CommonModule,
		MaterialModule,
		FormsModule,
		ReactiveFormsModule
	]
} )
export class SharedModule { }

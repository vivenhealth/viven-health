import { NgModule } from '@angular/core';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FirebaseUIModule } from 'firebaseui-angular';

@NgModule( {
	imports: [
		SharedModule,
		FirebaseUIModule,
		LazyLoadImageModule,
		LoginRoutingModule
	],
	declarations: [
		LoginComponent
	]
} )
export class LoginModule { }

import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Component( {
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [ './login.component.scss' ]
} )
export class LoginComponent implements OnInit {

	defaultImage = '../../../assets/images/bg-high-res.jpg';

	image = '../../../assets/images/bg-high-res.jpg';

	constructor( private afAuth: AngularFireAuth ) { }

	ngOnInit() {
		this.afAuth.authState.subscribe( user => {
			if ( user ) {
				console.log( 'login component', user );
			}
		} );
	}

}

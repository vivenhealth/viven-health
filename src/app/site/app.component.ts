import { Component, HostBinding, Inject, OnDestroy, OnInit } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import * as firebase from 'firebase/app';
import { User } from 'firebase/app';
import * as firebaseui from 'firebaseui';
// import { FirebaseApp } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

import { fadeInAnimation } from './app-animations';
import { AppParent } from 'app/site/app-parent';

import { FirebaseApp } from 'firebase-rxjs'
import { DBSchema } from './database-schema';

// import { VivenUser } from './entities/user.model';
import { UserService } from './entities/user.service';


@Component( {
	selector: 'app-root',
	template: `<div [@fadeInAnimation]="true"><router-outlet></router-outlet></div>`,
	animations: [ fadeInAnimation ]
} )
export class AppComponent extends AppParent implements OnInit, OnDestroy {

	@HostBinding( '@fadeInAnimation' )
	fadeInAnimation = false;

	constructor(
		private afAuth: AngularFireAuth,

		private afApp: FirebaseApp,

		private router: Router,

		private userService: UserService
	) {
		super();
	}

	ngOnInit() {

		// const db = this.afApp.database<DBSchema>();

		// Or if you go deeper in the tree pass a sub schema as the type parameter.
		// const usersRef = db.ref( '/users' );

		this.afAuth.authState.subscribe( user => {
			if ( user ) {
				console.log( 'app component', user );
				this.router.navigateByUrl( '/home' );
			} else {
				this.router.navigateByUrl( '' );
			}
		} );
	}

	/*
	ngOnInit(): void {

		this.afAuth.authState.subscribe( auth => {

			this.user = auth;

			console.log( this.user );

			// Temporary 'AuthGaurd'
			if ( auth ) {

				// Temporary placement for user node creation

				// Add/Update User
				this.userService.createNewUser( this.user ).subscribe( r => {
					console.log( 'USER ADDED/UPDATED' );
					this.router.navigateByUrl( '/master' );
				} );


			} else {
				this.router.navigateByUrl( '/login' );
			}

		} );
	}
	*/

	ngOnDestroy() {
		this.disposeSubscriptions();
	}
}

import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { VivenUser } from './user.model';
import { Observable } from 'rxjs/Observable';

/**
 * @description WIP. My experimental data service to model all other data services.
 *
 * @class UserService
 */
@Injectable()
export class UserService {

	private user: VivenUser;
	private user$: FirebaseObjectObservable<any>;
	private users$: FirebaseListObservable<any[]>;

	constructor(
		private afAuth: AngularFireAuth,
		private afDatabase: AngularFireDatabase
	) { }

	/*
	getUserById( uid: string ) {
		this.user$ = this.afDatabase.object( `/users/${ uid }`, { preserveSnapshot: true } );
		this.user$.subscribe( snapshot => {
			if ( snapshot.exists() ) {
				this.user = snapshot.val();
			}
		} );
	}
	*/

	getAllUsers() {
		this.users$ = this.afDatabase.list( '/users', { preserveSnapshot: true } );
		this.users$.subscribe( snapshot => {
			if ( snapshot.length > 0 ) {
				console.log( 'snap', snapshot );
				// this.users = snapshot.keys;
			}
		} );
	}

	createUser() {
		// Working on some firebase functions for this.
	}

	updateUser() {
		// Working on some firebase functions for this.
	}

	deleteUser() {
		// Working on some firebase functions for this.
	}

	createNewUser( userAuth: VivenUser ) {

		const user = JSON.parse( JSON.stringify( userAuth ) ) ;

		return this.getUserById( user.uid ).map(
			obj => {
				if ( !obj.$value ) {
					this.afDatabase
						.object( `users/${ user.uid }` )
						.set( user )
						.then(() => console.log( 'New VivenUser Added to DB' ) )
						.catch(() => console.clear() );
				}
				return this.updateUserAuth( userAuth );
			}
		);
	}

	getUserById( uid: string ) {
		return this.afDatabase.object( `users/${ uid }` );
	}

	updateUserAuth( userAuth: VivenUser ) {
		return this.getUserById( userAuth.uid ).switchMap(() => {
			// VivenUser is logged in, good time to update
			// 		the user here in case the user updates any info
			this.afDatabase
				.object( `users/${ userAuth.uid }` )
				.set( userAuth )
				.then(() => console.log( 'VivenUser added/updated in DB' ) )
				.catch(() => console.clear() );
			return Observable.of( userAuth );
		} );
	}
}

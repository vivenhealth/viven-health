import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class VivenUserService {

	usersRef = '/users';

	users$: FirebaseListObservable<any[]>;

	constructor( private afData: AngularFireDatabase, private afAuth: AngularFireAuth ) {
		this.users$ = this.afData.list( this.usersRef );

		/* // if we need to query
		this.users$ = this.af.list( '/users', {
			query: {
				limitToFirst: 3
			}
		} );
		*/
	}

	createUser( payload: any ): void {
		this.users$.push( payload );
	}

	updateUser( payload: any, newData: any ): void {
		this.afData.object( `${ this.usersRef }/${ payload.$key }` )
			.update( newData );
	}

	deleteUser( payload: any ): void {
		this.afData.object( `${ this.usersRef }/${ payload.$key }` ).remove();
	}

	toggleUserRole( payload: any ): void {
		this.afData.object( `${ this.usersRef }/${ payload.$key }` )
			.update( { isAdmin: !payload.isAdmin } );
	}

	getUserById( uid: string ) {
		return this.afData.object( `${ this.usersRef }/${ uid }` );
	}
}

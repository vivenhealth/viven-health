export class VivenUser {

	static fromJsonList( array ): VivenUser[] {
		return array.map( VivenUser.fromJson );
	}

	static fromJson( {
		$key,
		companyId,
		isAdmin,
		email,
		emailVerified,
		userName,
		firstName,
		lastName,
		fullName,
		photoUri,
		uid,
		createdAt,
		lastLoginAt,
		token,
		searchIndex,
		badgeId
	}
	): VivenUser {
		return new VivenUser(
			$key,
			companyId,
			isAdmin,
			email,
			emailVerified,
			userName,
			firstName,
			lastName,
			fullName,
			photoUri,
			uid,
			createdAt,
			lastLoginAt,
			token,
			searchIndex,
			badgeId
		);
	}

	constructor(
		public $key?: string,
		public companyId?: string,
		public isAdmin?: boolean,
		public email?: string,
		public emailVerified?: boolean,
		public userName?: string,
		public firstName?: string,
		public lastName?: string,
		public fullName?: string,
		public photoUri?: string,
		public uid?: string,
		public token?: string,
		public createdAt?: Object,
		public lastLoginAt?: Object,
		public searchIndex?: UserSearchIndex,
		public badgeId?: string
	) { }
}

export class UserSearchIndex {
	fullName: string;
	reversedFullName: string;

	constructor(
		fullName?: string,
		reversedFullName?: string,
	) {
		this.fullName = fullName;
		this.reversedFullName = reversedFullName;
	}
}

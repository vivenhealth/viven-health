import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AuthMethods, AuthProviders, FirebaseUIAuthConfig, FirebaseUIModule } from 'firebaseui-angular';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../../environments/environment';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { FirebaseRxJSModule } from 'firebase-rxjs-angular'

/* App Root */
import { AppComponent } from './app.component';

/* Feature Modules */
import { LoginModule } from './login/login.module';
import { CoreModule } from './core/core.module';

/* Routing Module */
import { AppRoutingModule } from './app-routing.module';


const firebaseUiAuthConfig: FirebaseUIAuthConfig = {
	providers: [
		AuthProviders.Google,
		AuthProviders.Facebook,
		AuthProviders.Twitter,
		// AuthProviders.Github,
		AuthProviders.Password,
		// AuthProviders.Phone
	],
	method: AuthMethods.Popup,
	tos: 'www.vivenhealth.com'
};

@NgModule( {
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpModule,
		LoginModule,
		CoreModule, // .forRoot( { userName: 'Jared Wuliger' } )
		AppRoutingModule,

		AngularFireModule.initializeApp( environment.firebaseConfig ),
		AngularFireAuthModule,
		AngularFireDatabaseModule,
		FirebaseUIModule.forRoot( firebaseUiAuthConfig ),

		FirebaseRxJSModule.primaryApp( {
			options: environment.firebaseConfig
		} )

	],
	exports: [
		BrowserAnimationsModule,
		HttpModule
	],
	declarations: [ AppComponent ],
	bootstrap: [ AppComponent ]
} )
export class AppModule { }

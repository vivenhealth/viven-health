import {
	ModuleWithProviders, NgModule,
	Optional, SkipSelf
} from '@angular/core';

import { CommonModule } from '@angular/common';

import { UserService } from '../entities/user.service';
import { MdIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@NgModule( {
	imports: [ CommonModule ],
	providers: [ UserService ]
} )
export class CoreModule {

	constructor( private iconRegistry: MdIconRegistry, private sanitizer: DomSanitizer, @Optional() @SkipSelf() parentModule: CoreModule ) {
		if ( parentModule ) {
			throw new Error(
				'CoreModule is already loaded. Import it in the AppModule only' );
		}
		iconRegistry.addSvgIcon( 'chevron-down', sanitizer.bypassSecurityTrustResourceUrl( 'assets/icons/chevron-down.svg' ) );
		iconRegistry.addSvgIcon( 'loader', sanitizer.bypassSecurityTrustResourceUrl( 'assets/icons/loader.svg' ) );
		iconRegistry.addSvgIcon( 'log-out', sanitizer.bypassSecurityTrustResourceUrl( 'assets/icons/log-out.svg' ) );
		iconRegistry.addSvgIcon( 'x', sanitizer.bypassSecurityTrustResourceUrl( 'assets/icons/x.svg' ) );
	}

	/*
	static forRoot( config: UserServiceConfig ): ModuleWithProviders {
		return {
			ngModule: CoreModule,
			providers: [
				{ provide: UserServiceConfig, useValue: config }
			]
		};
	}
	*/
}

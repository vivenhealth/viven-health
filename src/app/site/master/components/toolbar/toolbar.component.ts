import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

@Component( {
	selector: 'app-master-toolbar',
	templateUrl: './toolbar.component.html',
	styleUrls: [ './toolbar.component.scss' ]
} )
export class ToolbarComponent implements OnInit {

	public user$: Observable<firebase.User>;

	constructor( private afAuth: AngularFireAuth ) { }

	ngOnInit(): void {
		this.user$ = this.afAuth.authState;
	}

	logout() {
		this.afAuth.auth.signOut();
	}

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterComponent } from './master.component';
import { ProgramsComponent } from './containers/programs/programs.component';
import { ProgramDetailComponent } from './containers/programs/program-detail/program-detail.component';
import { ProgramsListComponent } from './containers/programs/program-list/programs-list.component';

const routes: Routes = [
	{ path: '', component: MasterComponent },
	{
		path: 'programs',
		component: ProgramsComponent,
		children: [
			{ path: '', component: ProgramsListComponent },
			{ path: 'program/:id', component: ProgramDetailComponent }
		]
	}
];

@NgModule( {
	imports: [ RouterModule.forChild( routes ) ],
	exports: [ RouterModule ]
} )
export class MasterRoutingModule { }

export const routedComponents = [
	MasterComponent,
	ProgramsComponent,
	ProgramsListComponent,
	ProgramDetailComponent
]

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramsComponent } from './programs.component';
import { ProgramDetailComponent } from './program-detail/program-detail.component';
import { ProgramsListComponent } from './program-list/programs-list.component';

@NgModule( {
	imports: [
		CommonModule
	],
	declarations: []
} )
export class ProgramsModule { }

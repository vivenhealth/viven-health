import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { MasterRoutingModule, routedComponents } from './master-routing.module';

import { ToolbarComponent } from './components/toolbar/toolbar.component';

@NgModule( {
	imports: [
		SharedModule,
		MasterRoutingModule
	],
	declarations: [
		ToolbarComponent,
		routedComponents
	]
} )
export class MasterModule { }

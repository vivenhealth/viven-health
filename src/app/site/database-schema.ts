// ** EXAMPLE ** //

export interface VivenUser {
	firstName: string
	lastName: string
	twitter: string
	profileImg: string
}

export interface Comment {
	text: string
	createdAt: number
	author: string
	authorId: string
}

export interface Comments {
	[ commentId: string ]: Comment
}

export interface BlogPost {
	title: string
	summary: string
	body: string
	author: string
	authorId: string
	publishedAt: number
	updatedAt: number
	comments: Comments
}

export interface DBSchema {
	users: {
		[ userId: string ]: VivenUser
	}
	blogPosts: {
		[ postId: string ]: BlogPost
	}
}

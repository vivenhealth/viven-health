import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// libs
import { bootloader } from '@angularclass/bootloader';

// app
import { AppModule } from './app/site/app.module';
import { environment } from './environments/environment';

if ( environment.production ) {
	enableProdMode();
}

export function main(): any {
	return platformBrowserDynamic().bootstrapModule( AppModule );
}

bootloader( main );
